package resuseableFL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class ActionPerform {
public String ActionPerformer(String LocatorType, String Action, WebDriver driver, String Locator, String value)
{
	String returnValue = "";
	switch(Action.toUpperCase())
	{
	case "CLICK":
		this.clickAction(LocatorType, driver, Locator);
		break;
	case "ENTERTEXT":
		this.exterText(LocatorType, driver, Locator, value);
		break;
	case "EXTRACTTEXT":
		returnValue = this.extractText(LocatorType, driver, Locator);
		break;
	case "DROPDOWNSELECT":
		this.dropDownSelect(LocatorType, driver, Locator, value);
		break;
	}
	return returnValue;
}
public void clickAction(String LocatorType , WebDriver driver, String Locator)
{
	switch(LocatorType.toUpperCase())
	{
	case "XPATH":
		driver.findElement(By.xpath(Locator)).click();
		break;
	case "NAME":
		driver.findElement(By.name(Locator)).click();
		break;
	case "LINKTEXT":
		driver.findElement(By.linkText(Locator)).click();
		break;
	case "ID":
		driver.findElement(By.id(Locator)).click();
		break;
	
	}
}


public void exterText(String LocatorType , WebDriver driver, String Locator, String value)
{
	switch(LocatorType.toUpperCase())
	{
	case "XPATH":
		driver.findElement(By.xpath(Locator)).sendKeys(value);
		break;
	case "NAME":
		driver.findElement(By.name(Locator)).sendKeys(value);
		break;
	case "LINKTEXT":
		driver.findElement(By.linkText(Locator)).sendKeys(value);
		break;
	case "ID":
		driver.findElement(By.id(Locator)).sendKeys(value);
		break;
	
	}
}

public String extractText(String LocatorType , WebDriver driver, String Locator)
{
	String returnValue = "";
	switch(LocatorType.toUpperCase())
	{
	case "XPATH":
		returnValue = driver.findElement(By.xpath(Locator)).getText();
		break;
	case "NAME":
		returnValue = driver.findElement(By.name(Locator)).getText();
		break;
	case "LINKTEXT":
		returnValue = driver.findElement(By.linkText(Locator)).getText();
		break;
	case "ID":
		returnValue = driver.findElement(By.id(Locator)).getText();
		break;
	
	}
	return returnValue;
}


public void dropDownSelect(String LocatorType , WebDriver driver, String Locator, String value)
{
	switch(LocatorType.toUpperCase())
	{
	case "XPATH":
		Select s = new Select(driver.findElement(By.xpath(Locator)));
		s.selectByValue(value);
		break;
	case "NAME":
		Select s1 = new Select(driver.findElement(By.name(Locator)));
		s1.selectByValue(value);
		break;
	case "LINKTEXT":
		Select s2 = new Select(driver.findElement(By.linkText(Locator)));
		s2.selectByValue(value);
		break;
	case "ID":
		Select s3 = new Select(driver.findElement(By.id(Locator)));
		s3.selectByValue(value);
		break;
	
	}
}
}
