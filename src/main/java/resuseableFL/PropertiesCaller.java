package resuseableFL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesCaller {

	//Source To ResourceFile String Path *HardCoded*
		String SrcToResourceFilesPath="\\src\\main\\resources\\ResourceFiles\\";
		
		public Properties FileInits(String fileName)
		{
		File file = new File(fileName);
		FileReader fis = null;
		try
		{
		 fis = new FileReader(file);
		}
		catch(FileNotFoundException ex)
		{
			System.out.println("Properties file not found at specified location");
			ex.printStackTrace();
		}
		Properties p = new Properties();
		try {
			p.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to load properties file");
			e.printStackTrace();
		}
		return p;
		}
	}
