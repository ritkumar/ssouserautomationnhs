package SSOTestCases;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import resuseableFL.ActionPerform;
import resuseableFL.BaseTestCase;
import resuseableFL.OpenBrowserAndLoadURL;
import resuseableFL.PropertiesCaller;

public class Mfrmls extends BaseTestCase{
	WebDriver driver=null;
	String baseUrl="https://www.newhomesourceprofessional.com/mfrmls";
	ActionPerform AP= new ActionPerform();
	String SrcToResourceFilesPath = "/src/main/resources/ResourceFiles/";
	PropertiesCaller pro=new PropertiesCaller();
	@Test
	public void mfrmlsLogin()
	{
		Properties p1=pro.FileInits(System.getProperty("user.dir")+SrcToResourceFilesPath+"Locators.properties");
		//Open The Browser and Launch the Application
		test=extent.createTest("mfrmlsLogin");
		OpenBrowserAndLoadURL obj= new OpenBrowserAndLoadURL(driver);
		 driver=obj.BrowserSelector(p1.getProperty("BrowserName"),baseUrl);
		//Window Maximizied
		 driver.manage().window().maximize();
		 //Click SignIn
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("CommonSignIn"),"");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(p1.getProperty("SignInButton"))));
		 //Click SignIn Button
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("SignInButton"),"");
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("mfrmlsUserName"))));
		 //Enter MFRMLS UserName
		 AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("mfrmlsUserName"),p1.getProperty("mfrmlsUid"));
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("mfrmlsPassword"))));
         //Enter MFRMLS Password
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("mfrmlsPassword"),"");
		 try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("mfrmlsPassword"),p1.getProperty("mfrmlsPwd"));
		 //Click On Login Button.
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("mfrmlsLogin"),"");
		//Validation
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("Welcome"))));
			String welcomeMsg=AP.ActionPerformer("XPATH","EXTRACTTEXT", driver,p1.getProperty("Welcome"),"");
			Assert.assertTrue(welcomeMsg.contains("Welcome"),"MFRMLS User Login Failed");
			System.out.println("MFRMLS User Login Sucessfully");
			driver.quit();
	}

}
