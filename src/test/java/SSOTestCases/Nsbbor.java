package SSOTestCases;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import resuseableFL.ActionPerform;
import resuseableFL.BaseTestCase;
import resuseableFL.OpenBrowserAndLoadURL;
import resuseableFL.PropertiesCaller;
public class Nsbbor extends BaseTestCase{
	WebDriver driver=null;
	String baseUrl="https://www.newhomesourceprofessional.com/nsbbor";
	ActionPerform AP= new ActionPerform();
	String SrcToResourceFilesPath = "/src/main/resources/ResourceFiles/";
	PropertiesCaller pro=new PropertiesCaller();
	@Test
	public void nsbborLogin()
	{
		Properties p1=pro.FileInits(System.getProperty("user.dir")+SrcToResourceFilesPath+"Locators.properties");
		//Open The Browser and Launch the Application
		test=extent.createTest("nsbborLogin");
		OpenBrowserAndLoadURL obj= new OpenBrowserAndLoadURL(driver);
		 driver=obj.BrowserSelector(p1.getProperty("BrowserName"),baseUrl);
		 //Window Maximizied
		 driver.manage().window().maximize();
		 //Click SignIn
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("CommonSignIn"),"");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(p1.getProperty("SignInButton"))));
		 //Click SignIn Button
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("SignInButton"),"");
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("nsbborUsername"))));
		 //Enter Abor UserName
		 AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("nsbborUsername"),p1.getProperty("nsbborUid"));
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("AborPassword"))));
         //Enter AborPassword
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("nsbborPassword"),"");
		 try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("nsbborPassword"),p1.getProperty("nsbborPwd"));
		 //Click On Login Button.
		 try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("nsbborLogin"),"");
		 //Select NewHomeSourceProfessional From Dashboard
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("nsbborDashboard"))));
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("nsbborDashboard"),"");	
		 String originalwindow=driver.getWindowHandle();
		 System.out.println("Original window is "+originalwindow);
		 Set<String>allWindow=driver.getWindowHandles();
		 Iterator<String>itr= allWindow.iterator();
		 while(itr.hasNext())
		 {
			 driver.switchTo().window(itr.next());
		 }
		//Validation
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("Welcome"))));
			String welcomeMsg=AP.ActionPerformer("XPATH","EXTRACTTEXT", driver,p1.getProperty("Welcome"),"");
			Assert.assertTrue(welcomeMsg.contains("Welcome"),"Nsbbor User Login Failed");
			System.out.println("Nsbbor User Login Sucessfully");
			driver.quit();
		
	}

}
