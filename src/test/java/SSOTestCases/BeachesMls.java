package SSOTestCases;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import resuseableFL.ActionPerform;
import resuseableFL.BaseTestCase;
import resuseableFL.OpenBrowserAndLoadURL;
import resuseableFL.PropertiesCaller;
public class BeachesMls extends BaseTestCase{
	WebDriver driver=null;
	String baseUrl="https://www.newhomesourceprofessional.com/beachesmls";
	ActionPerform AP= new ActionPerform();
	String SrcToResourceFilesPath = "/src/main/resources/ResourceFiles/";
	PropertiesCaller pro=new PropertiesCaller();
	@Test
	public void beachesMlsLogin()
	{
		Properties p1=pro.FileInits(System.getProperty("user.dir")+SrcToResourceFilesPath+"Locators.properties");
		//Open The Browser and Launch the Application
		test=extent.createTest("beachesMlsLogin");
		OpenBrowserAndLoadURL obj= new OpenBrowserAndLoadURL(driver);
		 driver=obj.BrowserSelector(p1.getProperty("BrowserName"),baseUrl);
		 //Window Maximizied
		 driver.manage().window().maximize();
		 //Click SignIn
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("CommonSignIn"),"");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(p1.getProperty("SignInButton"))));
		 //Click SignIn Button
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("SignInButton"),"");
	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("beachesmlsUsername"))));
		 AP.ActionPerformer("XPATH", "ENTERTEXT", driver,p1.getProperty("beachesmlsUsername"),p1.getProperty("beachesMlsUid"));
	 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("beachesmlsPassword"))));
		 try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("beachesmlsPassword"),"");
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("beachesmlsPassword"),p1.getProperty("beachesMlsPwd"));
		 try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("beachesmlsLogin"),"");
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("beachesMlsDashboard"))));
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("beachesMlsDashboard"),"");
		 String originalwindow=driver.getWindowHandle();
		 System.out.println("Original window is "+originalwindow);
		 Set<String>allWindow=driver.getWindowHandles();
		 Iterator<String>itr= allWindow.iterator();
		 while(itr.hasNext())
		 {
			 driver.switchTo().window(itr.next());
		 }
		 System.out.println("Child Window is "+driver.getTitle());
		//Validation
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("Welcome"))));
		String welcomeMsg=AP.ActionPerformer("XPATH","EXTRACTTEXT", driver,p1.getProperty("Welcome"),"");
		Assert.assertTrue(welcomeMsg.contains("Welcome"),"Beaches MLS User Login Failed");
		System.out.println("Beaches MLS User Login Sucessfully");
		driver.quit();
	}

}
