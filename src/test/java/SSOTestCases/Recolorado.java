package SSOTestCases;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import resuseableFL.ActionPerform;
import resuseableFL.BaseTestCase;
import resuseableFL.OpenBrowserAndLoadURL;
import resuseableFL.PropertiesCaller;
public class Recolorado extends BaseTestCase{
	WebDriver driver=null;
	String baseUrl="https://www.newhomesourceprofessional.com/recolorado";
	ActionPerform AP= new ActionPerform();
	String SrcToResourceFilesPath = "/src/main/resources/ResourceFiles/";
	PropertiesCaller pro=new PropertiesCaller();
	@Test
	public void recoloradoLogin()
	{
		Properties p1=pro.FileInits(System.getProperty("user.dir")+SrcToResourceFilesPath+"Locators.properties");
		//Open The Browser and Launch the Application
		test=extent.createTest("recoloradoLogin");
		OpenBrowserAndLoadURL obj= new OpenBrowserAndLoadURL(driver);
		 driver=obj.BrowserSelector(p1.getProperty("BrowserName"),baseUrl);
		 //Window Maximizied
		 driver.manage().window().maximize();
		 //Click SignIn
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("CommonSignIn"),"");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(p1.getProperty("SignInButton"))));
		 //Click SignIn Button
		    AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("SignInButton"),"");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("RecoloradoUsername"))));
			AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("Toremainthispage"),"");
			AP.ActionPerformer("XPATH", "ENTERTEXT", driver,p1.getProperty("RecoloradoUsername"),p1.getProperty("RecoloradoUid"));
			AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("RecoloradoPassword"),p1.getProperty("RecoloradoPwd"));
			AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("RecoloradoLogin"),"");
			//Validation
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("Welcome"))));
			String welcomeMsg=AP.ActionPerformer("XPATH","EXTRACTTEXT", driver,p1.getProperty("Welcome"),"");
			Assert.assertTrue(welcomeMsg.contains("Welcome"),"Recolorado User Login Failed");
			System.out.println("Recolorado User Login Sucessfully");
			driver.quit();
		
	}

}
