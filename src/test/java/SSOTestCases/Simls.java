package SSOTestCases;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import resuseableFL.ActionPerform;
import resuseableFL.BaseTestCase;
import resuseableFL.OpenBrowserAndLoadURL;
import resuseableFL.PropertiesCaller;
public class Simls extends BaseTestCase{
	WebDriver driver=null;
	String baseUrl="https://www.newhomesourceprofessional.com/simls";
	ActionPerform AP= new ActionPerform();
	String SrcToResourceFilesPath = "/src/main/resources/ResourceFiles/";
	PropertiesCaller pro=new PropertiesCaller();
	@Test
	public void simlsLogin()
	{
		Properties p1=pro.FileInits(System.getProperty("user.dir")+SrcToResourceFilesPath+"Locators.properties");
		//Open the browser and lanuch the application.
		test=extent.createTest("simlsLogin");
		OpenBrowserAndLoadURL obj= new OpenBrowserAndLoadURL(driver);
		 driver=obj.BrowserSelector(p1.getProperty("BrowserName"),baseUrl);
		 //Window Maximized.
		 driver.manage().window().maximize();
		 //Window Maximizied
		 driver.manage().window().maximize();
		 //Click SignIn
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("CommonSignIn"),"");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("SignInButton"))));
		 //Click SignIn Button
		 AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("SignInButton"),"");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("simlsUsername"))));
		AP.ActionPerformer("XPATH", "ENTERTEXT", driver,p1.getProperty("miamiUsername"),p1.getProperty("simlsUid"));
		AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("simlsPassword"),"");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		AP.ActionPerformer("XPATH","ENTERTEXT", driver,p1.getProperty("simlsPassword"),p1.getProperty("simlsPwd"));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("simlsLoginButton"),"");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("simlsEndTour"))));
		AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("simlsEndTour"),"");
		AP.ActionPerformer("XPATH","CLICK", driver,p1.getProperty("simlsDashboard"),"");
		 String originalwindow=driver.getWindowHandle();
		 System.out.println("Original window is "+originalwindow);
		 Set<String>allWindow=driver.getWindowHandles();
		 Iterator<String>itr= allWindow.iterator();
		 while(itr.hasNext())
		 {
			 driver.switchTo().window(itr.next());
		 }
		//Validation
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p1.getProperty("Welcome"))));
		String welcomeMsg=AP.ActionPerformer("XPATH","EXTRACTTEXT", driver,p1.getProperty("Welcome"),"");
		Assert.assertTrue(welcomeMsg.contains("Welcome"),"SIMLS User Login Failed");
		System.out.println("SIMLS User Login Sucessfully");
		driver.quit();
	}

}
